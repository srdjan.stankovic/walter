import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EmployeeListComponent } from './employees/employee-list/employee-list.component';
import { MainTemplateComponent } from './template/main-template/main-template.component';



const routes: Routes = [
  {path: '', redirectTo: 'list', pathMatch: 'full'},
  {path: '', component: MainTemplateComponent, children: [
    { path: 'list', component: EmployeeListComponent },
  ]}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
