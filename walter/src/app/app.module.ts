import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { NavbarComponent } from './template/navbar/navbar.component';
import { MainTemplateComponent } from './template/main-template/main-template.component';
import { EmployeeService } from './employees/employee.service';
import { EmployeeListComponent } from './employees/employee-list/employee-list.component';
import { EmployeeItemComponent } from './employees/employee-list/employee-item/employee-item.component';
import { EmployeeSearchComponent } from './employees/employee-search/employee-search.component';
import { FilterEmployeesPipe } from './employees/filter-employees.pipe';


@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    MainTemplateComponent,
    EmployeeListComponent,
    EmployeeItemComponent,
    EmployeeSearchComponent,
    FilterEmployeesPipe,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
  ],
  providers: [EmployeeService],
  bootstrap: [AppComponent]
})
export class AppModule { }
