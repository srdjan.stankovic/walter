import { Component, OnInit, Input } from '@angular/core';
import { Employee } from '../../employee.model';

@Component({
  selector: 'app-employee-item',
  templateUrl: './employee-item.component.html',
  styleUrls: ['./employee-item.component.scss']
})
export class EmployeeItemComponent implements OnInit {

  @Input() employee: Employee;
  editable: boolean = false;
  inputValue: string;

  constructor() { }

  ngOnInit() {
  }

  editRow() {
    this.editable = true;
  }

  saveRow() {
    this.editable = false;
    if (this.inputValue) {
      this.employee.name = this.inputValue;
    }
  }

  deleteRow() {
    this.employee.name = "";
  }
  
  changeName(event: any) {
    this.inputValue = event.target.value.trim();
  }
}
