import { Component, OnInit, Input } from '@angular/core';
import { Employee } from '../employee.model';
import { EmployeeService } from '../employee.service';

@Component({
  selector: 'app-employee-list',
  templateUrl: './employee-list.component.html',
  styleUrls: ['./employee-list.component.scss'],
})
export class EmployeeListComponent implements OnInit {

  employeeList: Employee[] = [];
  filterSearch: string;
  
  constructor(private _employeeService: EmployeeService) { }

  ngOnInit() {
    this._employeeService.getEmployees().subscribe(employees => {
      employees.forEach(employee => {
        this.employeeList.push(employee);
      })
    });

    this._employeeService.currentSearch.subscribe(search => this.filterSearch = search);
  }
}
