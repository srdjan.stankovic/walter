import { Component, OnInit } from '@angular/core';
import { EmployeeService} from '../employee.service';

@Component({
  selector: 'app-employee-search',
  templateUrl: './employee-search.component.html',
  styleUrls: ['./employee-search.component.scss']
})
export class EmployeeSearchComponent implements OnInit {

  filterSearch: string;

  constructor(private _employeeService: EmployeeService) { }

  ngOnInit() {
    this._employeeService.currentSearch.subscribe(search => this.filterSearch = search);
  }

  updateFilterSearch($event) {
    this._employeeService.updateFilterSearch($event.target.value);
  }

}
