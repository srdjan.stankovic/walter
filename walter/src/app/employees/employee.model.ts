export class Employee {
    
    public id: number;
    public name: string;
    public arrived: Date;
    public late: boolean;


    constructor(id: number, name: string, arrived: string) {
        this.id = id;
        this.name = name;
        this.arrived = new Date(arrived);
    }

}