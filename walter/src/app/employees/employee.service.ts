import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Employee } from './employee.model';
import { BehaviorSubject } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class EmployeeService {

  private filterSearch = new BehaviorSubject<string>(undefined);
  currentSearch = this.filterSearch.asObservable();

  constructor(private httpClient: HttpClient) { }

  getEmployees() {
    return this.httpClient.get<Array<Employee>>('./assets/data.json');
  }

  isLate(employee) {
    if (employee.arrived.getHours() > 8 && employee.arrived.getMinutes() > 45) {
        return true;
    } else {
        return false;
    }
  }

  updateFilterSearch(search: string) {
    this.filterSearch.next(search);
  }
  
}
