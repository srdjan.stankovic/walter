import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filterEmployees'
})
export class FilterEmployeesPipe implements PipeTransform {

  transform(employees: any, filterSearch: any): any {
    if (filterSearch == undefined) return employees;

    return employees.filter(employee => {
      return employee.name.toLowerCase().includes(filterSearch.toLowerCase());
    });
  }

}
